import { Get, JsonController, Param, Post } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

@JsonController('/flights')
export default class FlightsController {
    constructor(private readonly flightsService: FlightsService) {}

    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await this.flightsService.getAll(),
        }
    }
    @Post('/:flightId/passengers/:personId', { transformResponse: false })
    async onBoard(
        @Param('flightId') flightId: string,
        @Param('personId') personId: string
    ) {
        try {
            return {
                status: 200,
                data: await this.flightsService.onBoard(flightId, personId),
            }
        } catch (error: any) {
            return {
                status: 400,
                data: error.message,
            }
        }
    }
}
