import {
    Body,
    Delete,
    Get,
    JsonController,
    Param,
    Post,
} from 'routing-controllers'
import { CreatePersonDto } from '../dtos/create-person.dto'
import { PersonsService } from '../services/persons.service'

const personsService = new PersonsService()

@JsonController('/persons', { transformResponse: false })
export class PersonsController {
    @Get()
    async getAll() {
        const data = await personsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @Post('', { transformResponse: false })
    async create(@Body({ validate: true }) person: CreatePersonDto) {
        await personsService.create(person)
        return {
            status: 201,
        }
    }
    @Delete('/:id', { transformResponse: false })
    async delete(@Param('id') id: string) {
        try {
            await personsService.delete(id)
            return {
                status: 204,
            }
        } catch (error) {
            return {
                status: 500,
                error: 'Internal server error',
            }
        }
    }
}
