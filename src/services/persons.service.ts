import { CreatePersonDto } from '../dtos/create-person.dto'
import { PersonsModel } from '../models/persons.model'

export class PersonsService {
    getAll() {
        return PersonsModel.find()
    }

    async create(person: CreatePersonDto) {
        const exists = await PersonsModel.findOne({ email: person.email })
        if (exists) {
            throw new Error('Person already exists')
        }
        return PersonsModel.create(person)
    }

    delete(id: string) {
        return PersonsModel.findByIdAndDelete(id)
    }
}
