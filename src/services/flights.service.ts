import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async onBoard(flightId: string, personId: string) {
        const flight = await FlightsModel.findById(flightId)

        if (!flight) {
            throw new Error('Flight not found')
        }
        const person = await PersonsModel.findById(personId)
        if (!person) {
            throw new Error('Person not found')
        }
        if (flight.passengers.includes(personId)) {
            throw new Error('Person already on board')
        }
        flight.passengers.push(personId)
        await flight.save()
        console.log(flight)

        return flight
    }
}
