import { IsEmail, IsEnum, IsOptional, IsString } from 'class-validator'
import { Gender, Person } from '../models/persons.model'

export class CreatePersonDto implements Person {
    @IsString()
    name: string

    @IsEmail()
    email: string

    @IsEnum(Gender)
    gender: Gender

    @IsOptional()
    @IsString()
    type: string
}
