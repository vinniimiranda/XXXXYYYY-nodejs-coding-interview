import { CreatePersonDto } from '../src/dtos/create-person.dto'
import { db } from '../src/memory-database'
import { Gender } from '../src/models/persons.model'
import { PersonsService } from '../src/services/persons.service'

describe('Persons Model', () => {
    beforeAll(async () => {
        await db({ test: true })
    })
    const sut = new PersonsService()
    it('Allows to create two persons with different emails', async () => {
        const person: CreatePersonDto = {
            name: 'John Doe',
            email: 'joh.doe@email.com',
            gender: Gender.Male,
            type: '',
        }
        const antoherPerson: CreatePersonDto = {
            name: 'John Doe',
            email: 'joh.doe2@email.com',
            gender: Gender.Male,
            type: '',
        }

        await expect(sut.create(person)).resolves.not.toThrow()
        await expect(sut.create(antoherPerson)).resolves.not.toThrow()
        await expect(sut.getAll()).resolves.toHaveLength(2)
    })

    it('Prevents creating a person that already exists on the Database', async () => {
        const person = {
            name: 'John Doe',
            email: 'joh.doe@email.com',
            gender: 'Male',
        }

        await expect(sut.create(person as any)).resolves.not.toThrow()
        await expect(sut.create(person as any)).rejects.toThrow()
    })
})
